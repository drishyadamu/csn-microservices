package com.cg.boot.staffservice;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
class EventTest {
    
    @Test
     void testGettersAndSetters() {
        // Create an instance of Event
        Event event = new Event();
        
        // Set values using setters
        event.setId(1L);
        event.setEvent("Test Event");
        LocalDate date = LocalDate.now();
        event.setDateOfEvent(date);
        event.setPeriod(5);
        
        // Verify the values using getters
        assertEquals(1L, event.getId());
        assertEquals("Test Event", event.getEvent());
        assertEquals(date, event.getDateOfEvent());
        assertEquals(5, event.getPeriod());
    }
}
