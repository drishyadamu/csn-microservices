package com.cg.boot.chatservice;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@WebMvcTest(ChatController.class)
class ChatControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ApprovedRegistrationRepository approvedRegistrationRepository;

    @MockBean
    private MessageRepository messageRepository;

    @MockBean
    private UserServiceClient userServiceClient;

    @InjectMocks
    private ChatController chatController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(chatController).build();
    }

    @Test
     void testSendMessage_Success() throws Exception {
        // Mock the session and user
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInUser", new ApprovedRegistration());

        // Mock the recipient
        ApprovedRegistration recipient = new ApprovedRegistration();
        recipient.setId(2L);

        when(approvedRegistrationRepository.findById(recipient.getId())).thenReturn(Optional.of(recipient));

        // Create the message request
        ChatController.MessageRequest messageRequest = new ChatController.MessageRequest();
        messageRequest.setRecipientId(recipient.getId());
        messageRequest.setContent("Hello");

        // Perform the request
        mockMvc.perform(post("/chat/send")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(messageRequest))
                .session(session))
                .andExpect(status().isOk())
                .andExpect(content().string("Message sent to user " + recipient.getFirstName() + " successfully!"));

        // Verify that the message is saved
        verify(messageRepository, times(1)).save(any(Message.class));
    }

    @Test
     void testSendMessage_InvalidRecipientId() throws Exception {
        // Mock the session and user
        MockHttpSession session = new MockHttpSession();
        session.setAttribute("loggedInUser", new ApprovedRegistration());

        // Mock an invalid recipient ID
        Long invalidRecipientId = 999L;

        when(approvedRegistrationRepository.findById(invalidRecipientId)).thenReturn(Optional.empty());

        // Create the message request with an invalid recipient ID
        ChatController.MessageRequest messageRequest = new ChatController.MessageRequest();
        messageRequest.setRecipientId(invalidRecipientId);
        messageRequest.setContent("Hello");

        // Perform the request
        mockMvc.perform(post("/chat/send")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(messageRequest))
                .session(session))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Recipient with ID " + invalidRecipientId + " does not exist or is not an approved user."));

        // Verify that the message is not saved
        verify(messageRepository, never()).save(any(Message.class));
    }

    @Test
     void testGetMessagesForLoggedInUser_Success() throws Exception {
        // Mock the session and user
        MockHttpSession session = new MockHttpSession();
        ApprovedRegistration loggedInUser = new ApprovedRegistration();
        session.setAttribute("loggedInUser", loggedInUser);

        // Mock the sent messages
        List<Message> sentMessages = new ArrayList<>();
        sentMessages.add(new Message());
        sentMessages.add(new Message());

        when(messageRepository.findBySender(loggedInUser)).thenReturn(sentMessages);

        // Mock the received messages
        List<Message> receivedMessages = new ArrayList<>();
        receivedMessages.add(new Message());

        when(messageRepository.findByRecipient(loggedInUser)).thenReturn(receivedMessages);

        // Perform the request
        mockMvc.perform(get("/chat/messages").session(session))
                .andExpect(status().isOk());
               // .andExpect(jsonPath("$.sentMessages", hasSize(2)))
               // .andExpect(jsonPath("$.receivedMessages", hasSize(1)));
    }
    @Test
    void testSendGroupMessage_Success() {
        // Prepare test data
        GroupMessageRequest groupMessageRequest = new GroupMessageRequest();
        groupMessageRequest.setGroupIdentifier("groupId");
        groupMessageRequest.setContent("Hello, group!");
        groupMessageRequest.setRecipientIds(Arrays.asList(1L, 2L, 3L));

        ApprovedRegistration recipient1 = new ApprovedRegistration();
        recipient1.setId(1L);
        ApprovedRegistration recipient2 = new ApprovedRegistration();
        recipient2.setId(2L);
        ApprovedRegistration recipient3 = new ApprovedRegistration();
        recipient3.setId(3L);

        when(approvedRegistrationRepository.findAllById(groupMessageRequest.getRecipientIds()))
                .thenReturn(Arrays.asList(recipient1, recipient2, recipient3));

        // Execute the method
        ResponseEntity<?> response = chatController.sendGroupMessage(groupMessageRequest, null);

        // Verify the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Group message sent successfully!", response.getBody());
    }

    @Test
    void testUpdateMessage_Success() {
        // Prepare test data
        Long messageId = 1L;
        MessageUpdate messageUpdate = new MessageUpdate();
        messageUpdate.setContent("Updated content");

        Message existingMessage = new Message();
        existingMessage.setId(messageId);
        existingMessage.setContent("Old content");

        when(messageRepository.findById(messageId)).thenReturn(Optional.of(existingMessage));

        // Execute the method
        ResponseEntity<?> response = chatController.updateMessage(messageId, messageUpdate);

        // Verify the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Message updated successfully!", response.getBody());

        // Verify the updated message
        assertEquals(messageUpdate.getContent(), existingMessage.getContent());
        assertNotNull(existingMessage.getUpdatedAt());
    }

    @Test
    void testDeleteMessage_Success() {
        // Prepare test data
        Long messageId = 1L;

        when(messageRepository.findById(messageId)).thenReturn(Optional.of(new Message()));

        // Execute the method
        ResponseEntity<?> response = chatController.deleteMessage(messageId);

        // Verify the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Message deleted successfully!", response.getBody());

        // Verify that the message is deleted
        verify(messageRepository).deleteById(messageId);
    }

    @Test
     void testUpdateGroupMessage_Success() {
        // Prepare test data
        String groupId = "groupId";
        MessageUpdate messageUpdate = new MessageUpdate();
        messageUpdate.setContent("Updated content");

        Message message1 = new Message();
        message1.setId(1L);
        message1.setGroupIdentifier(groupId);
        Message message2 = new Message();
        message2.setId(2L);
        message2.setGroupIdentifier(groupId);
        Message message3 = new Message();
        message3.setId(3L);
        message3.setGroupIdentifier(groupId);

        when(messageRepository.findByGroupIdentifier(groupId))
                .thenReturn(Arrays.asList(message1, message2, message3));

        // Execute the method
        ResponseEntity<?> response = chatController.updateGroupMessage(groupId, messageUpdate);

        // Verify the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Group messages updated successfully!", response.getBody());

        // Verify the updated messages
        for (Message message : Arrays.asList(message1, message2, message3)) {
            assertEquals(messageUpdate.getContent(), message.getContent());
            assertNotNull(message.getUpdatedAt());
        }
    }

    @Test
    void testDeleteGroupMessage_Success() {
        // Prepare test data
        String groupId = "groupId";
        Message message1 = new Message();
        message1.setId(1L);
        message1.setGroupIdentifier(groupId);
        Message message2 = new Message();
        message2.setId(2L);
        message2.setGroupIdentifier(groupId);
        Message message3 = new Message();
        message3.setId(3L);
        message3.setGroupIdentifier(groupId);

        when(messageRepository.findByGroupIdentifier(groupId))
                .thenReturn(Arrays.asList(message1, message2, message3));

        // Execute the method
        ResponseEntity<?> response = chatController.deleteGroupMessage(groupId);

        // Verify the response
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Group messages deleted successfully!", response.getBody());

        // Verify that the messages are deleted
        verify(messageRepository).deleteAll(Arrays.asList(message1, message2, message3));
    }
    @Test
     void testSendGroupMessage_EmptyRecipientIds() {
        // Prepare test data
        GroupMessageRequest groupMessageRequest = new GroupMessageRequest();
        groupMessageRequest.setGroupIdentifier("groupId");
        groupMessageRequest.setContent("Hello, group!");
        groupMessageRequest.setRecipientIds(Collections.emptyList());

        // Execute the method
        ResponseEntity<?> response = chatController.sendGroupMessage(groupMessageRequest, null);

        // Verify the response
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Recipient IDs cannot be empty.", response.getBody());
    }

    @Test
    void testSendGroupMessage_InvalidRecipients() {
        // Prepare test data
        GroupMessageRequest groupMessageRequest = new GroupMessageRequest();
        groupMessageRequest.setGroupIdentifier("groupId");
        groupMessageRequest.setContent("Hello, group!");
        groupMessageRequest.setRecipientIds(Arrays.asList(1L, 2L, 3L));

        when(approvedRegistrationRepository.findAllById(groupMessageRequest.getRecipientIds()))
                .thenReturn(Collections.emptyList());

        // Execute the method
        ResponseEntity<?> response = chatController.sendGroupMessage(groupMessageRequest, null);

        // Verify the response
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Recipients do not exist or are not approved users.", response.getBody());
    }

    @Test
     void testUpdateMessage_NonExistentMessage() {
        // Prepare test data
        Long messageId = 1L;
        MessageUpdate messageUpdate = new MessageUpdate();
        messageUpdate.setContent("Updated content");

        when(messageRepository.findById(messageId)).thenReturn(Optional.empty());

        // Execute the method
        ResponseEntity<?> response = chatController.updateMessage(messageId, messageUpdate);

        // Verify the response
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Message with ID " + messageId + " does not exist.", response.getBody());
    }

    @Test
     void testDeleteMessage_NonExistentMessage() {
        // Prepare test data
        Long messageId = 1L;

        when(messageRepository.findById(messageId)).thenReturn(Optional.empty());

        // Execute the method
        ResponseEntity<?> response = chatController.deleteMessage(messageId);

        // Verify the response
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Message with ID " + messageId + " does not exist.", response.getBody());
    }

    @Test
    void testUpdateGroupMessage_NoMessagesFound() {
        // Prepare test data
        String groupId = "groupId";
        MessageUpdate messageUpdate = new MessageUpdate();
        messageUpdate.setContent("Updated content");

        when(messageRepository.findByGroupIdentifier(groupId)).thenReturn(Collections.emptyList());

        // Execute the method
        ResponseEntity<?> response = chatController.updateGroupMessage(groupId, messageUpdate);

        // Verify the response
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("No messages found for group with identifier " + groupId + ".", response.getBody());
    }

    @Test
    public void testDeleteGroupMessage_NoMessagesFound() {
        // Prepare test data
        String groupId = "groupId";

        when(messageRepository.findByGroupIdentifier(groupId)).thenReturn(Collections.emptyList());

        // Execute the method
        ResponseEntity<?> response = chatController.deleteGroupMessage(groupId);

        // Verify the response
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("No messages found for group with identifier " + groupId + ".", response.getBody());
    }

    @Test
    void testGetUserIdFromUserService_WhenUserLoggedIn() {
        // Create a mock HttpSession
        HttpSession session = Mockito.mock(HttpSession.class);
        
        // Create a mock ApprovedRegistration object
        ApprovedRegistration user = new ApprovedRegistration();
        user.setId(2L);

        // Set the mock user as the attribute in the session
        Mockito.when(session.getAttribute("loggedInUser")).thenReturn(user);



        // Call the method being tested
        long userId =chatController.getUserIdFromUserService(session);

        // Verify the result
        Assertions.assertEquals(2L, userId);
    } 

    @Test
    void testGetUserIdFromUserService_WhenUserNotLoggedIn() {
        // Create a mock HttpSession
        HttpSession session = Mockito.mock(HttpSession.class);

        // Set the attribute in the session as null to simulate the user not being logged in
        Mockito.when(session.getAttribute("loggedInUser")).thenReturn(null);

      
        // Call the method being tested
        long userId =chatController.getUserIdFromUserService(session);

        // Verify the result when the user is not logged in
        Assertions.assertEquals(1L, userId);
    }
    // Utility method to convert an object to JSON string
    private String asJsonString(Object object) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
