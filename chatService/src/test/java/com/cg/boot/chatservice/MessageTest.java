package com.cg.boot.chatservice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

public class MessageTest {

    @Test
    public void testSettersAndGetters() {
        // Create a Message object
        Message message = new Message();
        
        // Set values using setters
        Long id = 1L;
        long sender = 2L;
        ApprovedRegistration recipient = new ApprovedRegistration();
        String content = "Hello, world!";
        LocalDateTime sentAt = LocalDateTime.now();
        LocalDateTime updatedAt = LocalDateTime.now();
        String groupIdentifier = "groupId";

        message.setId(id);
        message.setSender(sender);
        message.setRecipient(recipient);
        message.setContent(content);
        message.setSentAt(sentAt);
        message.setUpdatedAt(updatedAt);
        message.setGroupIdentifier(groupIdentifier);

        // Use getters to retrieve the values
        Assertions.assertEquals(id, message.getId());
      //  Assertions.assertEquals(sender, message.getSender());
        Assertions.assertEquals(recipient, message.getRecipient());
        Assertions.assertEquals(content, message.getContent());
        Assertions.assertEquals(sentAt, message.getSentAt());
        Assertions.assertEquals(updatedAt, message.getUpdatedAt());
        Assertions.assertEquals(groupIdentifier, message.getGroupIdentifier());
    }
}
