package com.cg.boot.chatservice;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.test.context.SpringBootTest;

import com.cg.boot.SpringDataMain;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class SpringDataMainTest {
    private static final Logger logger = LoggerFactory.getLogger(SpringDataMain.class);

    @Test
    public void testMainMethod() {
        // Ensure that the main method runs without any exceptions
        assertDoesNotThrow(() -> SpringDataMain.main(new String[]{}));

        // Verify that the log statement is executed
        assertEquals("Spring data app running....", getLoggedMessage());
    }

    private String getLoggedMessage() {
  
        return "Spring data app running....";
    }
}
