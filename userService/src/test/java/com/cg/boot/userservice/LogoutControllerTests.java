package com.cg.boot.userservice;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(LogoutController.class)
 class LogoutControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MockHttpSession session;

//    @Test
//    public void testLogout() throws Exception {
//        // Perform a POST request to /logout
//        MockHttpServletRequestBuilder request = post("/logout")
//                .session(session)
//                .contentType(MediaType.APPLICATION_JSON);
//
//        // Simulate a successful logout
//        mockMvc.perform(request)
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.content().string("Logout successful"));
//
//        // Simulate an error during logout
//        mockMvc.perform(request)
//                .andExpect(status().isBadRequest())
//                .andExpect(MockMvcResultMatchers.content().string("User is not logged in"));
//    }

    
    @Test
    void testLogoutUserNotLoggedIn() throws Exception {
        // Set the session attribute to null to simulate a user not being logged in
        session.setAttribute("loggedInUser", null);

        // Perform a POST request to /logout
        mockMvc.perform(post("/logout").session(session).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().string("User is not logged in"));
    }
}
