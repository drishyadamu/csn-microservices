package com.cg.boot.userservice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
 class RegisteredTryTest {

    @Test
     void testGettersAndSetters() {
        RegisteredTry registeredTry = new RegisteredTry();
        Long id = 1L;
        String firstName = "John";
        String lastName = "Doe";
        Integer age = 25;
        String username = "johndoe";
        String password = "password";
        String role = "user";

        registeredTry.setId(id);
        registeredTry.setFirstName(firstName);
        registeredTry.setLastName(lastName);
        registeredTry.setAge(age);
        registeredTry.setUsername(username);
        registeredTry.setPassword(password);
        registeredTry.setRole(role);

        Assertions.assertEquals(id, registeredTry.getId());
        Assertions.assertEquals(firstName, registeredTry.getFirstName());
        Assertions.assertEquals(lastName, registeredTry.getLastName());
        Assertions.assertEquals(age, registeredTry.getAge());
        Assertions.assertEquals(username, registeredTry.getUsername());
        Assertions.assertEquals(password, registeredTry.getPassword());
        Assertions.assertEquals(role, registeredTry.getRole());
    }
    
    // Add more test methods to cover other functionality of the RegisteredTry class
}
