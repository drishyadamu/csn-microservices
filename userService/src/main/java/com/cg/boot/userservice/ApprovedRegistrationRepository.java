package com.cg.boot.userservice;

import java.util.Optional;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApprovedRegistrationRepository extends JpaRepository<ApprovedRegistration, Long> {
    ApprovedRegistration findByUsernameAndPasswordAndRole(String username, String password, String role);

	

	Optional<ApprovedRegistration> findByUsername(String username);
}