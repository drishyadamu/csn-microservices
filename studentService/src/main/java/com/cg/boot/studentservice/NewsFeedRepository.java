package com.cg.boot.studentservice;

import org.springframework.data.jpa.repository.JpaRepository;


import org.springframework.stereotype.Repository;
@Repository
public interface NewsFeedRepository extends JpaRepository<NewsFeed, Long> {

	

}

