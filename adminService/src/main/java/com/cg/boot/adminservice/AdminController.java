package com.cg.boot.adminservice;

import java.util.Optional;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private RegisteredTryRepository registeredTryRepository;

	@Autowired
	private ApprovedRegistrationRepository approvedRegistrationRepository;

	@Autowired
	private NewsFeedRepository newsFeedRepository;



	@Autowired
	private ApprovedNewsFeedRepository approvedNewsFeedRepository;

	private static final String ACCESS_DENIED_MESSAGE = "Access denied. Please login as admin.";
	private static final String LOGIN_MESSAGE = "loggedInUser";


    @GetMapping("/users")
    public ResponseEntity<?> getUsers(HttpSession session) {
        Iterable<RegisteredTry> users = registeredTryRepository.findAll();
        return ResponseEntity.ok(users);
    }

    @GetMapping("/newsfeed")
    public ResponseEntity<?> getNewsFeed(HttpSession session) {
        Iterable<NewsFeed> newsFeed = newsFeedRepository.findAll();
        return ResponseEntity.ok(newsFeed);
    }

    @PostMapping("/users/{id}/approve")
    public ResponseEntity<?> approveUser(@PathVariable("id") Long id, HttpSession session) {
        RegisteredTry user = registeredTryRepository.findById(id).orElse(null);
        if (user != null) {
            ApprovedRegistration approvedRegistration = new ApprovedRegistration();
            approvedRegistration.setFirstName(user.getFirstName());
            approvedRegistration.setLastName(user.getLastName());
            approvedRegistration.setAge(user.getAge());
            approvedRegistration.setUsername(user.getUsername());
            approvedRegistration.setPassword(user.getPassword());
            approvedRegistration.setRole(user.getRole());
            approvedRegistrationRepository.save(approvedRegistration);
            registeredTryRepository.delete(user);
            return ResponseEntity.ok("User " + user.getUsername() + " has been approved and added to the approved registrations.");
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @PostMapping("/news-feed/approve/{id}")
    public ResponseEntity<?> approveNewsFeed(@PathVariable Long id, HttpSession session) {
        ApprovedRegistration admin = (ApprovedRegistration) session.getAttribute(LOGIN_MESSAGE);
        if (admin != null) {
            Optional<NewsFeed> newsFeed = newsFeedRepository.findById(id);
            if (newsFeed.isPresent()) {
                ApprovedNewsFeed approvedNewsFeed = new ApprovedNewsFeed();
                approvedNewsFeed.setTitle(newsFeed.get().getTitle());
                approvedNewsFeed.setContent(newsFeed.get().getContent());
                approvedNewsFeed.setPostedDate(newsFeed.get().getPostedDate());
                approvedNewsFeed.setStudentId(newsFeed.get().getStudentId());
                approvedNewsFeedRepository.save(approvedNewsFeed);
                newsFeedRepository.deleteById(id);
                return ResponseEntity.ok("News feed with ID " + id + " has been approved and moved to the approved news feed database.");
            } else {
                return ResponseEntity.badRequest().body("News feed with ID " + id + " does not exist.");
            }
        } else {
            return ResponseEntity.badRequest().body(ACCESS_DENIED_MESSAGE);
        }
    }
}


